import './App.css';
import {Component} from "react";
import StartButton from "../components/StartButton/StartButton";
import {v1 as uuid} from "uuid";
import Board from "../components/Board/Board";
import Attempts from "../components/Attempts/Attempts";
import ResetButton from "../components/ResetButton/ResetButton";
import FinishGame from "../components/FinishGame/FinishGame";

class App extends Component {
  state = {
    startClicked: false,
    attempts: 0,
    itemFound: false
  };

  getRandomIndex = () => {
    const index = Math.floor(Math.random() * 36);
    return index;
  };

  createCellsArray = () => {
    let newCells = [];
    for (let i = 0; i < 36; i++) {
      newCells.push({id: uuid(), hasItem: false, clicked: false});
    }
    const index = this.getRandomIndex();
    newCells[index].hasItem = true;
    this.setState({cells: newCells, startClicked: true});
  };

  printBoard = () => {
    this.createCellsArray();
  };

  isClicked = (id) => {
    const cells = [...this.state.cells];
    let attempts = this.state.attempts;
    const index = cells.findIndex(cell => cell.id === id);
    cells[index].clicked = true;
    attempts++;
    if (cells[index].hasItem) {
      this.setState({itemFound: true});
    }
    this.setState({cells, attempts});

  };

  resetClicked = () => {
    this.createCellsArray();
    this.setState({attempts: 0, itemFound: false});
  };

  render() {
    let titleClass = ["Title"];

    return (
      <div className="App">
        <header className="App-header">
         <h1 className={titleClass}>Let's play, find the Ring!</h1>
        </header>
        <div className="container">
          <StartButton
            startButtonClicked={this.state.startClicked}
            startClicked={this.printBoard}
          />
          {
            this.state.cells ?
              <div>
                <Board
                  cells={this.state.cells}
                  itemFound={this.state.itemFound}
                  cellClicked={this.isClicked}
                />
                <Attempts
                  tries={this.state.attempts}
                />
                <ResetButton
                  itemFound={this.state.itemFound}
                  resetClicked={this.resetClicked}
                />
                {
                  this.state.itemFound ?
                    <FinishGame />
                    : null
                }
              </div>
              : null
          }

        </div>

      </div>
    );
  }
}

export default App;
