import React from "react";
import "./Attempts.css"

const Attempts = props => {
  return (
    <p className="attempts">
      Tries: <span className="number">{props.tries}</span>
    </p>
  );
};

export default Attempts;