import React from "react";
import "./ResetButton.css"

const ResetButton = (props) => {
  let resetBtn = ["Button"];
  if (props.itemFound) {
    resetBtn.push("ResetButton");
  }
  return (
    <div className="btn_wrapper">
      <button className={resetBtn.join(" ")} onClick={props.resetClicked}>
        Reset
      </button>
    </div>
  );
};

export default ResetButton;