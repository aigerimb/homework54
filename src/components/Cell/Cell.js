import React from "react";
import "./Cell.css"

const Cell = props => {
  let cellClass = ["cell"];
  if (props.cell.clicked === true) {
    cellClass.push("clickedCell");
  };

  if (props.cell.hasItem === true) {
    cellClass.push("hiddenItem");
  }

  return (
    <div className={cellClass.join(" ")} onClick={props.cellClicked}> </div>
  );
};

export default Cell;