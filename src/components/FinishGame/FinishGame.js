import React from "react";
import "./FinishGame.css";
import image from "./img/ring.png"

const FinishGame = () => {
  return (
    <div className="FinishGame">
      <h2>Congrats! You found the ring. </h2>
      <img src={image} className="ring-icon" alt="Ring Image"/>
      <h3>Want to play again? Click "RESET" button!</h3>
    </div>
  );
};

export default FinishGame;