import React from "react";
import "./Board.css"
import Cell from "../Cell/Cell";

const Board = props => {
  let boardClass = ["Board"]
  if (props.itemFound === true) {
    boardClass.push("disableBoard");
  }
  return (
    <div className={boardClass.join(" ")}>
      {
        props.cells.map(cell => {
          return <Cell
            key={cell.id}
            cell={cell}
            cellClicked={() => props.cellClicked(cell.id)}/>

        })
      }
    </div>
  )
};

export default Board;