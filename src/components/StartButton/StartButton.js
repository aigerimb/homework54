import React from "react";
import "./StartButton.css"

const StartButton = props => {
  let startBtn = ["Button"];
  if (props.startButtonClicked === true) {
    startBtn.push("hideStartButton");
  }
  return (
    <div className="btn_wrapper">
      <button className={startBtn.join(" ")} onClick={props.startClicked}>Start</button>
    </div>
  );
};

export default StartButton;